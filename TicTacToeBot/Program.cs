﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TicTacToeBot
{
    class Node <T>
    {
        public List<Node<T>> filhos;
        public string valor;
        public Node()
        {
            filhos = new List<Node<T>>();
        }
        public int score;
    }

    static class Program
    {

        public static string ReplaceAt(this string str, int index, int length, string replace)
        {
            return str.Remove(index, Math.Min(length, str.Length - index))
                    .Insert(index, replace);
        }

        public static void Playo(char player, Node<string> game, char originalPlayer)
        {
            //if (!game.valor.Contains('_'))
            if (Winner(game))
            {
                return;
            }

            
            for (int i = 0; i < 9; i++)
            {
                var jogada = game.valor;
                if (game.valor[i].Equals('_'))
                {
                    jogada = ReplaceAt(game.valor, i, 1, player.ToString());

                    var child = new Node<string>() { valor = jogada };
                    game.filhos.Add(child);
                    Playo(player.Equals('x') ? 'o' : 'x', child, originalPlayer);
                    game.score = MiniMax(game, player, originalPlayer);
                }
            }
                  

        }

        public static bool Winner(Node<string> estado) 
        {

            if ((estado.valor[0] == estado.valor[1] && estado.valor[1] == estado.valor[2] && estado.valor[2] != '_') || //horizontal
                (estado.valor[3] == estado.valor[4] && estado.valor[4] == estado.valor[5] && estado.valor[5] != '_') ||
                (estado.valor[6] == estado.valor[7] && estado.valor[7] == estado.valor[8] && estado.valor[8] != '_') ||
                                                                                                             
                (estado.valor[0] == estado.valor[4] && estado.valor[4] == estado.valor[8] && estado.valor[8] != '_') || //diagonais
                (estado.valor[2] == estado.valor[4] && estado.valor[4] == estado.valor[6] && estado.valor[6] != '_') ||
                                                                                                            
                (estado.valor[0] == estado.valor[3] && estado.valor[3] == estado.valor[6] && estado.valor[6] != '_') ||//verticais
                (estado.valor[1] == estado.valor[4] && estado.valor[4] == estado.valor[7] && estado.valor[7] != '_') ||
                (estado.valor[2] == estado.valor[5] && estado.valor[5] == estado.valor[8] && estado.valor[8] != '_'))
                return true;
            return false;
        }

        public static bool Draw(Node<string> estado, char player)
        {
            if (!estado.valor.Contains('_') && Winner(estado) == false)
            {
                return true;
            }
            return false;
        }

        public static int Score(Node<string> estado, char player) 
        {
            if ((estado.valor[0] == estado.valor[1] && estado.valor[1] == estado.valor[2] && estado.valor[2] == player) || //horizontal
                (estado.valor[3] == estado.valor[4] && estado.valor[4] == estado.valor[5] && estado.valor[5] == player) ||
                (estado.valor[6] == estado.valor[7] && estado.valor[7] == estado.valor[8] && estado.valor[8] == player) ||

                (estado.valor[0] == estado.valor[4] && estado.valor[4] == estado.valor[8] && estado.valor[8] == player) || //diagonais
                (estado.valor[2] == estado.valor[4] && estado.valor[4] == estado.valor[6] && estado.valor[6] == player) ||

                (estado.valor[0] == estado.valor[3] && estado.valor[3] == estado.valor[6] && estado.valor[6] == player) ||//verticais
                (estado.valor[1] == estado.valor[4] && estado.valor[4] == estado.valor[7] && estado.valor[7] == player) ||
                (estado.valor[2] == estado.valor[5] && estado.valor[5] == estado.valor[8] && estado.valor[8] == player))
                return 1;
            return -1;
        }

        //public static Node<string> MiniMax(Node<string> estado)
        //{
        //    if (estado.filhos.Count == 0 || estado.score != 0 )
        //    {
        //        return estado;
        //    }
                
        //    else
        //    {
        //        var best = new Node<string>();
        //        foreach (var child in estado.filhos)
        //        {
        //            best = MiniMax(child);
        //            if (best.score > estado.score)
        //                {
        //                    return best;
        //                }
        //        }
        //        return best;
        //    }
        //}


        //http://snipd.net/minimax-algorithm-with-alpha-beta-pruning-in-c

        public static int MiniMax(Node<string> node, char player, char originalPlayer)
        {
            if (node.filhos.Count == 0 || Winner(node))
                return Score(node, player);
            if (player == originalPlayer)
            {
                var a = 0;
                foreach (var f in node.filhos) 
                {
                    a = a + MiniMax(f, player, originalPlayer);
                }
                return a;
            }
            else
            {
                var a = 0;
                foreach (var f in node.filhos)
                {
                    a = a - MiniMax(f, player, originalPlayer);
                }
                return a;
            }
        }

        //public static int Iterate(Node<string> node, int alpha, int beta, bool PlayerMax, char Player)
        //{
        //    if (node.filhos.Count == 0 || Winner(node))
        //    {
        //        return Score(node, Player);
        //    }

        //    if (PlayerMax == true)
        //    {
        //        foreach (Node<string> child in node.filhos)
        //        {
        //            alpha = Math.Max(alpha, Iterate(child, alpha, beta, PlayerMax, Player));
        //            if (beta < alpha)
        //            {
        //                break;
        //            }

        //        }

        //        return alpha;
        //    }
        //    else
        //    {
        //        foreach (Node<string> child in node.filhos)
        //        {
        //            beta = Math.Min(beta, Iterate(child, alpha, beta, !PlayerMax, Player));

        //            if (beta < alpha)
        //            {
        //                break;
        //            }
        //        }

        //        return beta;
        //    }
        //}

        static void Main(string[] args)
        {

            var game = new Node<string>() { filhos = new List<Node<string>>(), valor="_________" };
            var game1 = new Node<string>() { filhos = new List<Node<string>>(), valor = "xxxoo____" };
            char player = 'x';


            Playo(player, game, player);
        }
    }

}
